﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CefService.Model;
using CefService.Utils;

namespace CefService.JSON {

	public class PhoneCallJsonConverter : JsonConverter<PhoneCall> {

		#region Static Properties
		private static JavaScriptSerializer serializer;
		private static PhoneCallJsonConverter instance;

		private static JavaScriptSerializer JSONSerailizer {
			get {
				if (serializer == null) {
					serializer = new JavaScriptSerializer();
				}
				return serializer;
			}
		}

		public static PhoneCallJsonConverter SharedInstance {
			get {
				if (instance == null) {
					instance = new PhoneCallJsonConverter();
				}
				return instance;
			}
		}
		#endregion

		#region Quick Shortcut Methods
		public static PhoneCall FromJsonString(string jsonString) {
			Dictionary<string, object> result = (Dictionary<string, object>)JSONSerailizer.DeserializeObject(jsonString);
			return SharedInstance.FromDictionary(result);
		}

		public static string ToJsonString(PhoneCall call) {
			Dictionary<string, object> jsonObject = SharedInstance.AsDictionary(call);
			return JSONSerailizer.Serialize(jsonObject);
		}
		#endregion

		#region JsonConverter Methods
		public Dictionary<string, object> AsDictionary(PhoneCall call) {
			Dictionary<string, object> phoneCallObject = new Dictionary<string, object>();
			phoneCallObject.Add("caller", UserJsonConverter.SharedInstance.AsDictionary(call.Caller));
			phoneCallObject.Add("calledOn", call.CalledOn);
			phoneCallObject.Add("answeredOn", call.AnsweredOn);
			phoneCallObject.Add("endOn", call.EndOn);
			phoneCallObject.Add("state", call.State);
			return phoneCallObject;
		}

		public PhoneCall FromDictionary(Dictionary<string, object> input) {
			Dictionary<string, object> userObject = (Dictionary<string, object>)input["caller"];
			User caller = UserJsonConverter.SharedInstance.FromDictionary(userObject);
			PhoneCall call = new PhoneCall(caller);
			call.AnsweredOn = DictionaryUtils.ConvertIfExists(input, "answeredOn", Convert.ToDateTime);
			call.CalledOn = DictionaryUtils.ConvertIfExists(input, "calledOn", Convert.ToDateTime);
			call.EndOn = DictionaryUtils.ConvertIfExists(input, "endOn", Convert.ToDateTime);
			call.State = DictionaryUtils.ConvertIfExists(input, "state", (s) => { return (PhoneCall.PhoneCallState)s; });
			return call;
		}
		#endregion
	}
}
