﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CefService.Model;

namespace CefService.JSON {

	public class UserJsonConverter : JsonConverter<User> {

		#region Static Properties
		private static UserJsonConverter instance;
		private static JavaScriptSerializer serializer;

		private static JavaScriptSerializer JSONSerailizer {
			get {
				if (serializer == null) {
					serializer = new JavaScriptSerializer();
				}
				return serializer;
			}
		}
		public static UserJsonConverter SharedInstance {
			get {
				if (instance == null) {
					instance = new UserJsonConverter();
				}
				return instance;
			}
		}
		#endregion

		#region Quick Shortcut Methods
		public static User FromJsonString(string jsonString) {
			Dictionary<string, object> result = (Dictionary<string, object>)JSONSerailizer.DeserializeObject(jsonString);
			return SharedInstance.FromDictionary(result);
		}

		public static string ToJsonString(User user) {
			UserJsonConverter converter = new UserJsonConverter();
			Dictionary<string, object> jsonObject = converter.AsDictionary(user);
			return JSONSerailizer.Serialize(jsonObject);
		}
		#endregion

		#region JsonConverter Methods
		public Dictionary<string, object> AsDictionary(User user) {
			return AsDictionary(user);
		}

		public Dictionary<string, object> AsDictionary(UserInfo user) {
			Dictionary<string, object> jsonObject = new Dictionary<string, object>();
			jsonObject.Add("firstName", user.FirstName);
			jsonObject.Add("lastName", user.LastName);
			jsonObject.Add("identity", user.Identity);
			jsonObject.Add("photo", user.Photo);
			return jsonObject;
		}

		public User FromDictionary(Dictionary<string, object> input) {
			string firstName = input["firstName"]?.ToString();
			string lastName = input["lastName"]?.ToString();
			string identity = input["identity"]?.ToString();
			string photo = input["photo"]?.ToString();
			return new User(identity, firstName, lastName, photo);
		}
		#endregion
	}
}
