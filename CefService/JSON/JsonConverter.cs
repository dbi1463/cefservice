﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefService.JSON {

	public interface JsonConverter<T> {

		Dictionary<string, object> AsDictionary(T input);

		T FromDictionary(Dictionary<string, object> input);
	}
}
