﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CefService.Model;
using CefService.Properties;
using CefService.View;

namespace CefService {

	class CefServiceContext : ApplicationContext {

		#region Members
		private MainForm mainForm;
		private NotifyIcon trayIcon;

		private CefServiceModel mainModel;
		#endregion

		#region Properties
		public MainForm ActiveMainForm {
			get {
				if (mainForm == null || mainForm.IsDisposed) {
					mainForm = new MainForm(mainModel);
				}
				return mainForm;
			}
		}
		#endregion

		#region Constructors
		public CefServiceContext() {
			trayIcon = new NotifyIcon() {
				Icon = Icon.FromHandle(Resources.SystemTray.GetHicon()),
				ContextMenu = new ContextMenu(new MenuItem[] {
					new MenuItem("Open", ShowMainForm),
					new MenuItem("Exit", Exit),
					new MenuItem("Show Dev Tool", ShowDevTool)
				}),
				Visible = true
			};
			mainModel = new CefServiceModel();
			mainModel.CallComing += ShowNotificationFormForComingPhoneCall;
			ActiveMainForm.Show();
		}
		#endregion

		#region Menu Item Handlers
		void ShowMainForm(object sender, EventArgs e) {
			ActiveMainForm.ShowOnTop();
		}

		void Exit(object sender, EventArgs e) {
			Application.Exit();
		}

		void ShowDevTool(object sender, EventArgs e) {
			ActiveMainForm.ShowOnTop();
			ActiveMainForm.ShowDevTool();
		}
		#endregion

		#region CefServiceModel Event Handlers
		private void ShowNotificationFormForComingPhoneCall(PhoneCall call) {
			ActiveMainForm.Invoke((MethodInvoker) delegate {
				NotificationForm notification = new NotificationForm(mainModel, call);
				notification.Show();
			});
		}
		#endregion
	}
}
