﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefService.Model {

	public class PhoneCall {

		public enum PhoneCallState {
			Calling, Cancelled, Answered, NotAnswered, Refused
		}

		#region
		public UserInfo Caller {
			get; protected set;
		}

		public DateTime CalledOn {
			get; set;
		}

		public DateTime AnsweredOn {
			get; set;
		}

		public DateTime EndOn {
			get; set;
		}

		public PhoneCallState State {
			get; set;
		}
		#endregion

		#region Constructors
		public PhoneCall(UserInfo caller) {
			Caller = caller;
			State = PhoneCallState.Calling;
		}
		#endregion
	}
}
