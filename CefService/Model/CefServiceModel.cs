﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefService.Model {

	public class CefServiceModel {

		public delegate void PhoneCallComingHandler(PhoneCall call);

		public delegate void PhoneCallAnsweredHandler(PhoneCall call);

		public delegate void PhoneCallRefusedHandler(PhoneCall call);

		public delegate void PhoneCallTimeOutHandler(PhoneCall call);

		public delegate void PhoneCallCancelledHandler(PhoneCall call);

		public event PhoneCallComingHandler CallComing;

		public event PhoneCallAnsweredHandler CallAnswered;

		public event PhoneCallRefusedHandler CallRefused;

		public event PhoneCallTimeOutHandler CallTimeOut;

		public event PhoneCallCancelledHandler CallCancelled;

		public Dictionary<string, PhoneCall> CallingPhones {
			get;
			private set;
		}

		public CefServiceModel() {
			CallingPhones = new Dictionary<string, PhoneCall>();
		}

		#region Methods for Callee
		public void Answer(PhoneCall call) {
			call.State = PhoneCall.PhoneCallState.Answered;
			call.AnsweredOn = DateTime.Now;
			CallingPhones.Remove(call.Caller.Identity);
			CallAnswered?.Invoke(call);
		}

		public void Refuse(PhoneCall call) {
			call.State = PhoneCall.PhoneCallState.Refused;
			call.AnsweredOn = DateTime.Now;
			CallingPhones.Remove(call.Caller.Identity);
			CallRefused?.Invoke(call);
		}

		public void NotAnswer(PhoneCall call) {
			call.State = PhoneCall.PhoneCallState.NotAnswered;
			CallingPhones.Remove(call.Caller.Identity);
			CallTimeOut?.Invoke(call);
		}
		#endregion

		#region Methods for Caller
		public void NewPhoneCall(UserInfo caller) {
			PhoneCall call = new PhoneCall(caller);
			CallingPhones.Add(caller.Identity, call);
			CallComing?.Invoke(call);
		}

		public void CancelPhoneCall(UserInfo caller) {
			if (CallingPhones.ContainsKey(caller.Identity)) {
				PhoneCall call = CallingPhones[caller.Identity];
				CallingPhones.Remove(caller.Identity);
				CallCancelled?.Invoke(call);
			}
		}
		#endregion
	}
}
