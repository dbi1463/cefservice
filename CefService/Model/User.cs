﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefService.Model {

	public class User : UserInfo {

		#region Properties
		public string Identity {
			get; set;
		}

		public string FirstName {
			get; set;
		}

		public string LastName {
			get; set;
		}

		public string Photo {
			get; set;
		}
		#endregion

		#region Constructors
		public User(string identity, string firstName, string lastName, string photo) {
			Identity = identity;
			FirstName = firstName;
			LastName = lastName;
			Photo = photo;
		}
		#endregion
	}
}
