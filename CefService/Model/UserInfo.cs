﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefService.Model {

	public interface UserInfo {

		#region Properties
		string Identity {
			get;
		}

		string FirstName {
			get;
		}

		string LastName {
			get;
		}

		string Photo {
			get;
		}
		#endregion
	}
}
