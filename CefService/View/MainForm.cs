﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CefService.JSON;
using CefService.Model;
using CefSharp;
using CefSharp.WinForms;

namespace CefService.View {

	public class MainForm : Form {

		#region Win32 API
		private const int WM_SYSCOMMAND = 0x112;
		private const int MF_STRING = 0x0;
		private const int MF_SEPARATOR = 0x800;

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool AppendMenu(IntPtr hMenu, int uFlags, int uIDNewItem, string lpNewItem);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool InsertMenu(IntPtr hMenu, int uPosition, int uFlags, int uIDNewItem, string lpNewItem);
		#endregion

		private int SWITCH_BROWSER_ID = 0x1;
		private ChromiumWebBrowser browser;
		private WebBrowser ieBrowser;

		#region Properties
		public CefServiceModel MainModel {
			get; protected set;
		}

		public bool IEBrowserOnTop {
			get; set;
		}
		#endregion

		#region Constructors
		public MainForm(CefServiceModel model) {
			MainModel = model;
			IEBrowserOnTop = false;
			MainModel.CallAnswered += UserAnsweredPhoneCall;
			MainModel.CallRefused += UserRefusedPhoneCall;
			MainModel.CallTimeOut += UserIgnoredPhoneCall;
			InitializeComponents();
		}
		#endregion

		#region Public Methods
		public void ShowOnTop() {
			Show();
			if (WindowState == FormWindowState.Minimized) {
				WindowState = FormWindowState.Normal;
				browser.Invalidate();
			}
			Location = new Point(10, 10);
		}

		public void ShowOutsideScreen() {
			Screen screen = Screen.AllScreens.OrderBy(s => s.WorkingArea.Right).Last();
			Show();
			Location = new Point(screen.WorkingArea.Width, screen.WorkingArea.Height);
		}
		public void ShowDevTool() {
			browser.ShowDevTools();
		}
		#endregion

		#region Overriden Methods
		protected override void OnClosing(CancelEventArgs e) {
			e.Cancel = true;
			Hide();
		}

		protected override void OnHandleCreated(EventArgs e) {
			base.OnHandleCreated(e);
			IntPtr systemMenu = GetSystemMenu(Handle, false);
			AppendMenu(systemMenu, MF_SEPARATOR, 0, string.Empty);
			AppendMenu(systemMenu, MF_STRING, SWITCH_BROWSER_ID, "Swtich Browser");
		}

		protected override void WndProc(ref Message m) {
			base.WndProc(ref m);
			if (m.Msg == WM_SYSCOMMAND && (int)m.WParam == SWITCH_BROWSER_ID) {
				Controls.SetChildIndex(ieBrowser, IEBrowserOnTop ? 1 : 0);
				Controls.SetChildIndex(browser, IEBrowserOnTop ? 0 : 1);
				IEBrowserOnTop = !IEBrowserOnTop;
			}
		}
		#endregion

		#region Private Methods
		private void InitializeComponents() {
			SetClientSizeCore(1024, 768);
			InitializeChromiumBrowser();
			InitializeIEBrowser();
			ShowInTaskbar = true;
		}

		private void InitializeIEBrowser() {
			ieBrowser = new WebBrowser();
			ieBrowser.Dock = DockStyle.Fill;
			ieBrowser.Navigate("http://www.hncb.com.tw/");
			Controls.Add(ieBrowser);
		}

		private void InitializeChromiumBrowser() {
			browser = new ChromiumWebBrowser("");
			browser.RegisterJsObject("phoneCallCallbacks", new CefPhoneCallCallbacks(MainModel));
			browser.Dock = DockStyle.Fill;
			browser.LoadingStateChanged += HideFormAfterBrowserFinishLoading;
			//Panel panel = new Panel();
			//panel.Dock = DockStyle.Fill;
			//panel.Controls.Add(browser);
			Controls.Add(browser);
			browser.Load("https://dl.dropboxusercontent.com/u/19418059/phone_call.html");
		}
		#endregion

		#region
		private void HideFormAfterBrowserFinishLoading(object sender, LoadingStateChangedEventArgs e) {
			if (e.IsLoading == false) {
				Console.WriteLine("All resources have loaded, hide the main form");
				Invoke((MethodInvoker) delegate {
					Hide();
				});
			}
		}
		#endregion

		#region CefServiceModel Event Handlers
		private void UserIgnoredPhoneCall(PhoneCall call) {
			string json = PhoneCallJsonConverter.ToJsonString(call);
			Console.WriteLine("The phone call ({0}) is ignored", json);
			browser.ExecuteScriptAsync(string.Format("phoneCallIgnored('{0}')", json));
		}

		private void UserRefusedPhoneCall(PhoneCall call) {
			string json = PhoneCallJsonConverter.ToJsonString(call);
			Console.WriteLine("User refused the phone call {0}", json);
			browser.ExecuteScriptAsync(string.Format("phoneCallRefused('{0}')", json));
		}

		private void UserAnsweredPhoneCall(PhoneCall call) {
			string json = PhoneCallJsonConverter.ToJsonString(call);
			Console.WriteLine("User answered the phone call {0}", json);
			browser.ExecuteScriptAsync(string.Format("phoneCallAnswered('{0}')", json));
			ShowOnTop();
		}
		#endregion
	}
}
