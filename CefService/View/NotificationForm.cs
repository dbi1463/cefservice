﻿using System;
using System.Linq;
using System.Windows.Forms;
using CefService.Model;

namespace CefService.View {

	class NotificationForm : Form {

		#region Public Properties
		public CefServiceModel Model {
			get; protected set;
		}
		public PhoneCall ComingPhoneCall {
			get; protected set;
		}
		#endregion

		#region Members
		private Button answerButton;
		private Button refuseButton;
		private Label callerNameLabel;
		private Timer timer;
		#endregion

		#region Constructors
		public NotificationForm(CefServiceModel model, PhoneCall comingPhoneCall) {
			Model = model;
			ComingPhoneCall = comingPhoneCall;
			InitializeComponents();
			ActivateTimer();
		}
		#endregion

		#region Public Methods
		protected override void OnLoad(EventArgs e) {
			ControlBox = false;
			SetClientSizeCore(180, 120);
			Screen screen = Screen.AllScreens.OrderBy(s => s.WorkingArea.Right).Last();
			
			Left = screen.WorkingArea.Right - Size.Width;
			Top = screen.WorkingArea.Bottom - Size.Height;
			WindowState = FormWindowState.Normal;
			TopMost = true;
			ShowInTaskbar = false;
			Console.WriteLine("show on {0}, {1}", Left, Top);
			base.OnLoad(e);
		}
		#endregion

		#region Private Methods
		private void InitializeComponents() {
			Panel controlsBar = new Panel();
			controlsBar.Dock = DockStyle.Bottom;
			controlsBar.SetBounds(0, 70, 150, 30);

			answerButton = new Button();
			answerButton.Text = "Answer";
			answerButton.Dock = DockStyle.Right;
			controlsBar.Controls.Add(answerButton);

			refuseButton = new Button();
			refuseButton.Text = "Refuse";
			refuseButton.Dock = DockStyle.Left;
			controlsBar.Controls.Add(refuseButton);
			Controls.Add(controlsBar);

			callerNameLabel = new Label();
			callerNameLabel.Dock = DockStyle.Fill;
			callerNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			callerNameLabel.Text = String.Format("{0} {1} is calling", ComingPhoneCall.Caller.FirstName, ComingPhoneCall.Caller.LastName);
			Controls.Add(callerNameLabel);

			answerButton.Click += AnswerPhoneCall;
			refuseButton.Click += RefusePhoneCall;
		}

		private void ActivateTimer() {
			DeactivateTimer();
			timer = new Timer();
			timer.Interval = 10000;
			timer.Tick += TimerTimeOut;
			timer.Start();
		}

		private void DeactivateTimer() {
			if (timer != null) {
				timer.Stop();
				timer.Dispose();
				timer = null;
			}
		}

		private void CloseAndDeactivateTimer() {
			DeactivateTimer();
			Close();
		}
		#endregion

		#region Event Handlers
		private void AnswerPhoneCall(object sender, EventArgs e) {
			CloseAndDeactivateTimer();
			Model.Answer(ComingPhoneCall);
		}

		private void RefusePhoneCall(object sender, EventArgs e) {
			CloseAndDeactivateTimer();
			Model.Refuse(ComingPhoneCall);
		}

		private void TimerTimeOut(object sender, EventArgs e) {
			CloseAndDeactivateTimer();
			Model.NotAnswer(ComingPhoneCall);
		}
		#endregion
	}
}
