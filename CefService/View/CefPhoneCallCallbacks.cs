﻿using System;
using CefService.JSON;
using CefService.Model;

namespace CefService.View {

	class CefPhoneCallCallbacks {

		public CefServiceModel MainModel {
			get; private set;
		}

		public CefPhoneCallCallbacks(CefServiceModel model) {
			MainModel = model;
		}

		public void NewPhoneCallComing(string callerJsonInfo) {
			Console.WriteLine("New phone call back from the JavaScript: " + callerJsonInfo);
			User caller = UserJsonConverter.FromJsonString(callerJsonInfo);
			MainModel.NewPhoneCall(caller);
		}

		public void PhoneCallCancelled(string callerJsonInfo) {
			Console.WriteLine("Phone call cancelled from the JavaScript");
			User caller = UserJsonConverter.FromJsonString(callerJsonInfo);
			MainModel.CancelPhoneCall(caller);
		}
	}
}
