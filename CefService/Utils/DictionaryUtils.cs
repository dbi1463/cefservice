﻿using System;
using System.Collections.Generic;

namespace CefService.Utils {

	public class DictionaryUtils {

		public static OutputType ConvertIfExists<KeyType, ValueType, OutputType>(Dictionary<KeyType, ValueType> dictionary, KeyType key, Func<ValueType, OutputType> transformer) {
			return ConvertIfExists(dictionary, key, transformer, default(OutputType));
		}

		public static OutputType ConvertIfExists<KeyType, ValueType, OutputType>(Dictionary<KeyType, ValueType> dictionary, KeyType key, Func<ValueType, OutputType> transformer, OutputType defaultValue) {
			OutputType result = defaultValue;
			if (dictionary.ContainsKey(key)) {
				result = transformer.Invoke(dictionary[key]);
			}
			return result;
		}
	}
}
