﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using CefSharp;

namespace CefService {

	static class CefService {

		/// <summary>
		/// The main entry of the CefService.
		/// </summary>
		[STAThread]
		static void Main(string[] args) {
			if (args.Length == 1 && args[0] == "INSTALLER") {
				Process.Start(Application.ExecutablePath);
				return;
			}
			CefSettings settings = new CefSettings();
			Cef.Initialize(settings);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			CefServiceContext context = new CefServiceContext();
			Application.Run(context);
		}
	}
}
